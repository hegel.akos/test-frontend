import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import DatePicker from 'react-datepicker';
import { createTodo, getUserTodos, updateTodo, deleteTodo } from '../common/Todos';
import 'react-datepicker/dist/react-datepicker.css';
import './TodoModal.css';

export default function TodoModal(props) {
  Modal.setAppElement('#root');
  const initialNewTodoState = {
    title: '',
    date: new Date(),
  };
  const [userId] = useState(props.userId);
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState(initialNewTodoState);

  async function fetchUserTodos() {
    const userTodos = await getUserTodos(userId);
    setTodos(userTodos);
  }

  useEffect(() => {
    fetchUserTodos();
  }, [userId]);

  function onNewTodoTitleChange(event) {
    setNewTodo({ ...newTodo, title: event.target.value });
  }

  function onNewTodoDateChange(selectedDate) {
    setNewTodo({ ...newTodo, date: new Date(selectedDate) });
  }

  async function onTodoTitleChanged(id, title) {
    const response = await updateTodo(id, { title });
    updateTodos(response.id, response.title, response.date);
  }

  async function onTodoDateChanged(id, date) {
    const response = await updateTodo(id, { date: new Date(date).toISOString() });
    updateTodos(response.id, response.title, response.date);
  }

  async function deleteUserTodo(todoId) {
    await deleteTodo(todoId);
    setTodos([...todos.filter((todo) => todo.id !== todoId)]);
  }

  function updateTodos(id, title, date) {
    setTodos(
      todos.map((todo) => {
        if (todo.id === id) {
          return {
            ...todo,
            title,
            date,
          };
        }
        return todo;
      }),
    );
  }

  async function createNewTodo() {
    const createdTodo = await createTodo(userId, newTodo.title, newTodo.date);
    setTodos([...todos, createdTodo]);
  }

  function exitModal() {
    props.closeModal();
  }

  return (
    <div>
      <Modal isOpen={props.modalOpen}>
        <div className="todo_list">
          <div className="line">
            <div className="item">
              <span>
                <strong>Todo title:</strong>
              </span>
              <input
                placeholder="Example todo title"
                type="text"
                onChange={onNewTodoTitleChange}
                value={newTodo.title}
              />
            </div>
            <div className="item">
              <span>
                <strong>Date:</strong>
              </span>
              <DatePicker selected={newTodo.date} onChange={onNewTodoDateChange} />
            </div>
            <div className="item">
              <button onClick={createNewTodo}>Add new</button>
            </div>
          </div>
          {todos.map((todo) => (
            <div key={todo.id} className="line">
              <div className="item">
                <span>
                  <strong>Todo title:</strong>
                </span>
                <input type="text" value={todo.title} onChange={(e) => onTodoTitleChanged(todo.id, e.target.value)} />
              </div>
              <div className="item">
                <span>
                  <strong>Date:</strong>
                </span>
                <DatePicker
                  selected={new Date(todo.date)}
                  onChange={(newDate) => onTodoDateChanged(todo.id, newDate)}
                />
              </div>
              <div className="item">
                <button
                  onClick={() => {
                    deleteUserTodo(todo.id);
                  }}
                >
                  Delete
                </button>
              </div>
            </div>
          ))}
          <div>
            <button onClick={exitModal}>Exit modal</button>
          </div>
        </div>
      </Modal>
    </div>
  );
}
