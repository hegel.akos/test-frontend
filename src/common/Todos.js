import axios from 'axios';
import { backendServerUrl } from '../index';

export const getUserTodos = async (userId) => {
  const response = await axios.get(`${backendServerUrl}/users/${userId}/todos`);
  return response.data || [];
};

export const createTodo = async (userId, title, date) => {
  const response = await axios.post(`${backendServerUrl}/todos`, {
    userId,
    title,
    date,
  });
  return response.data;
};

export const updateTodo = async (todoId, data) => {
  const response = await axios.patch(`${backendServerUrl}/todos/${todoId}`, data);
  return response.data;
};

export const deleteTodo = async (todoId) => {
  await axios.delete(`${backendServerUrl}/todos/${todoId}`);
};
