import React, { useState, useEffect } from 'react';
import './App.css';
import User from './components/User.js';
import { getAllUsers, createUser } from './common/Users.js';

function App() {
  const initialNewUserState = {
    email: '',
    name: '',
    phoneNumber: '',
  };
  const [reload] = useState(0);
  const [users, setUsers] = useState([]);
  const [newUser, setNewUser] = useState(initialNewUserState);

  useEffect(() => {
    const fetchUsers = async () => {
      const users = await getAllUsers();
      setUsers(users);
    };
    fetchUsers();
  }, [reload]);

  function newUserEmailChange(event) {
    setNewUser({ ...newUser, email: event.target.value });
  }

  function newUserNameChange(event) {
    setNewUser({ ...newUser, name: event.target.value });
  }

  function newUserPhoneChange(event) {
    setNewUser({ ...newUser, phoneNumber: event.target.value });
  }

  async function createNewUser() {
    const createdUser = await createUser(newUser.email, newUser.name, newUser.phoneNumber);
    setUsers([...users, createdUser]);
    setNewUser(initialNewUserState);
  }

  function deleteUser(userId) {
    setUsers(users.filter((user) => user.id !== userId));
  }

  return (
    <div>
      <h1>Todo App</h1>
      <div className="user_list">
        <div className="user_holder">
          <div className="item">
            <span>Email:</span>
            <input placeholder="domain@example.com" type="email" value={newUser.email} onChange={newUserEmailChange} />
          </div>
          <div className="item">
            <span>Name:</span>
            <input placeholder="Name Test" type="text" value={newUser.name} onChange={newUserNameChange} />
          </div>
          <div className="item">
            <span>Phone:</span>
            <input placeholder="061234567891" type="text" value={newUser.phoneNumber} onChange={newUserPhoneChange} />
          </div>
          <div className="item">
            <button onClick={createNewUser}>Add new user</button>
          </div>
        </div>
        {users.map((user) => (
          <User key={user.id} user={user} deleteUser={deleteUser} />
        ))}
      </div>
    </div>
  );
}

export default App;
