import React, { useEffect, useState, useRef } from 'react';
import {
  getUserImage,
  updateUser,
  requestImageChange,
  uploadImage,
  updateUserImage,
  deleteUser,
} from '../common/Users.js';
import './User.css';
import TodoModal from './TodoModal.js';

export default function User(props) {
  const [modalOpen, setModalOpen] = useState(false);
  const [userState, setUserState] = useState(props.user);
  const [userImage, setUserImage] = useState('');
  const [selectedImage, setSelectedImage] = useState(null);

  function handeNameChange(data) {
    setUserState({ ...userState, name: data.target.value });
  }

  function handlePhoneChange(data) {
    setUserState({ ...userState, phoneNumber: data.target.value });
  }

  async function getImage(userId, imageId) {
    if (imageId) {
      const signedUrl = await getUserImage(userId, imageId);
      setUserImage(signedUrl.url);
    }
  }

  function saveUser() {
    updateUser(userState.id, userState.name, userState.phoneNumber).then((response) => {
      setUserState(response);
    });
  }

  const inputFile = useRef(null);

  function onImageClick() {
    inputFile.current.click();
  }

  function onUserImageChanged(data) {
    setSelectedImage(data.target.files[0] || null);
  }

  async function changeUserImage() {
    if (selectedImage) {
      const response = await requestImageChange(userState.id);
      await uploadImage(response.signedUrl, selectedImage);
      await updateUserImage(userState.id, response.ref);
      setUserState({ ...userState, image: response.ref });
    }
  }

  async function delUser(userId) {
    await deleteUser(userId);
    props.deleteUser(userId);
  }

  function openModal() {
    setModalOpen(true);
  }

  function closeModal() {
    setModalOpen(false);
  }

  useEffect(() => {
    getImage(userState.id, userState.image);
  }, [userState]);

  useEffect(() => {
    changeUserImage();
  }, [selectedImage]);

  return (
    <div className="user_holder">
      <div className="item">
        <img onClick={onImageClick} alt="" src={userImage} />
        <input onChange={onUserImageChanged} type="file" id="file" ref={inputFile} style={{ display: 'none' }} />
      </div>
      <div className="item">
        <span>Email:</span>
        <input type="email" readOnly={true} value={userState.email} />
      </div>
      <div className="item">
        <span>Name:</span>
        <input type="text" value={userState.name} onChange={handeNameChange} />
      </div>
      <div className="item">
        <span>Phone:</span>
        <input type="text" value={userState.phoneNumber} onChange={handlePhoneChange} />
      </div>
      <div className="item">
        <button onClick={saveUser}>Save</button>
      </div>
      <div className="item">
        <button onClick={openModal}>Show todos</button>
      </div>
      <div className="item">
        <button onClick={() => delUser(userState.id)}>Delete</button>
      </div>
      <div>
        <TodoModal modalOpen={modalOpen} closeModal={closeModal} userId={userState.id} />
      </div>
    </div>
  );
}
