import { backendServerUrl } from '../index';
import axios from 'axios';

export const getAllUsers = async () => {
  const response = await axios.get(`${backendServerUrl}/users`);
  return response.data || [];
};

export const getUserImage = async (userId, imageId) => {
  const response = await axios.get(`${backendServerUrl}/users/${userId}/images/${imageId}`);
  return response.data;
};

export const updateUser = async (userId, name, phoneNumber) => {
  const response = await axios.patch(`${backendServerUrl}/users/${userId}`, {
    name,
    phoneNumber,
  });
  return response.data;
};

export const requestImageChange = async (userId) => {
  const response = await axios.post(`${backendServerUrl}/users/${userId}/images`);
  return response.data;
};

export const uploadImage = (signedUrl, file) => {
  return axios.put(signedUrl, file);
};

export const updateUserImage = (userId, ref) => {
  const response = axios.patch(`${backendServerUrl}/users/${userId}/images`, { ref });
  return response.data;
};

export const deleteUser = async (userId) => {
  await axios.delete(`${backendServerUrl}/users/${userId}`);
};

export const createUser = async (email, name, phoneNumber) => {
  const response = await axios.post(`${backendServerUrl}/users`, {
    email,
    name,
    phoneNumber,
  });
  return response.data;
};
